DELIMITER //
CREATE PROCEDURE crearTablaJugadores()
BEGIN
    CREATE TABLE jugadores(
                           id int primary key auto_increment,
                           nombre varchar(30),
                           altura varchar(30),
                           peso varchar(30),
                           fecha_matriculacion timestamp,
                           posicion varchar(20),
                           club varchar(50));
END //
