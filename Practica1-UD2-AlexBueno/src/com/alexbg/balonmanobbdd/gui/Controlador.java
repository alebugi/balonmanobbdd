package com.alexbg.balonmanobbdd.gui;

import javax.swing.*;
import javax.swing.event.TableModelEvent;
import javax.swing.event.TableModelListener;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.sql.ResultSet;
import java.sql.SQLException;

/**
 * Created by Alex on 27/12/2020.
 */


public class Controlador implements ActionListener, TableModelListener{
    private Vista vista;
    private Modelo modelo;

    private enum tipoEstado {conectado, desconectado};
    private tipoEstado estado;

    public Controlador(Vista vista, Modelo modelo) {
        this.modelo = modelo;
        this.vista = vista;
        estado = tipoEstado.desconectado;

        iniciarTabla();
        iniciarTabla1();
        iniciarTabla2();

        addActionListener(this);
        addTableModelListeners(this);
    }

    private void addActionListener(ActionListener listener){
        vista.buscarButton.addActionListener(listener);
        vista.eliminarButton.addActionListener(listener);
        vista.jugadorPorClubButton.addActionListener(listener);
        vista.buscarPorEquipoYButton.addActionListener(listener);
        vista.nuevoButton.addActionListener(listener);
        vista.itemConectar.addActionListener(listener);
        vista.itemCrearTabla.addActionListener(listener);
        vista.itemSalir.addActionListener(listener);
    }

    private void addTableModelListeners(TableModelListener listener){
        vista.dtm.addTableModelListener(listener);
        vista.dtm1.addTableModelListener(listener);
        vista.dtm2.addTableModelListener(listener);
    }

    @Override
    public void tableChanged(TableModelEvent e) {
        if (e.getType() ==TableModelEvent.UPDATE) {
            System.out.println("actualizada");
            int filaModicada=e.getFirstRow();

            try {
                modelo.modificarPersona((Integer)vista.dtm.getValueAt(filaModicada, 0), (String)vista.dtm.getValueAt(filaModicada, 1),
                        (String)vista.dtm.getValueAt(filaModicada, 2), (String)vista.dtm.getValueAt(filaModicada, 3),
                        (java.sql.Timestamp)vista.dtm.getValueAt(filaModicada, 4), (String)vista.dtm.getValueAt(filaModicada, 5),
                        (String)vista.dtm.getValueAt(filaModicada,6));
                vista.lblAccion.setText("Columna actualizada");
            } catch (SQLException e1) {
                e1.printStackTrace();
            }
        }
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        String comando = e.getActionCommand();

        switch(comando){
            case "CrearTablaJugadores":
                try {
                    modelo.crearTablaJugadores();
                    vista.lblAccion.setText("Tabla jugadores creada");
                } catch (SQLException e1) {
                    e1.printStackTrace();
                }

                break;
            case "Nuevo":
                try {
                    modelo.insertarPersona(vista.txtNombre.getText(),vista.txtAltura.getText(),vista.txtPeso.getText(),
                            vista.dateTimePicker.getDateTimePermissive(),vista.txtPosicion.getText(),vista.txtClub.getText());
                    cargarFilas(modelo.obtenerDatos());
                } catch (SQLException e1) {
                    e1.printStackTrace();
                }
                break;
            case "Jugador por club":
                try{
                    modelo.jugadoresPorClub();
                    cargarFilas1(modelo.obtenerDatos1());
                } catch (SQLException e1){
                    e1.printStackTrace();
                }
                break;
            case "Buscar por nombre":
                String buscar=vista.txtBuscar.getText();
                try {
                    ResultSet rs =modelo.buscarJugador(buscar);
                    cargarFilas(rs);
                } catch (SQLException ex) {
                    ex.printStackTrace();
                }
                break;
            case "Buscar por equipo y/o posicion":
                String buscarEquipo = vista.txtBuscarEquipo.getText();
                String buscarPuesto = vista.txtBuscarPosicion.getText();
                try {
                    ResultSet rs = modelo.jugadoresEquipo(buscarEquipo, buscarPuesto);
                    cargarFilas2(rs);
                } catch (SQLException ex) {
                    ex.printStackTrace();
                }
                break;
            case "Eliminar":
                try {
                    int filaBorrar=vista.tabla.getSelectedRow();
                    int idBorrar= (Integer) vista.dtm.getValueAt(filaBorrar,0);
                    modelo.eliminarPersona(idBorrar);
                    vista.dtm.removeRow(filaBorrar);
                    vista.lblAccion.setText("Fila Eliminada");
                } catch (SQLException e1) {
                    e1.printStackTrace();
                }
                break;
            case "Salir":
                System.exit(0);
                break;
            case "Conectar":
                if (estado==tipoEstado.desconectado)  {
                    try {
                        modelo.conectar();
                        vista.itemConectar.setText("Desconectar");
                        estado=tipoEstado.conectado;
                        cargarFilas(modelo.obtenerDatos());
                    } catch (SQLException e1) {
                        JOptionPane.showMessageDialog(null,"Error de conexión","Error",JOptionPane.ERROR_MESSAGE);
                        e1.printStackTrace();
                    }
                } else {
                    try {
                        modelo.desconectar();
                        vista.itemConectar.setText("Conectar");
                        estado=tipoEstado.desconectado;
                        vista.lblAccion.setText("Desconectado");
                    } catch (SQLException e1) {
                        JOptionPane.showMessageDialog(null,"Error de desconexión","Error",JOptionPane.ERROR_MESSAGE);
                        e1.printStackTrace();
                    }
                }
                break;
        }
    }

    private void iniciarTabla(){
        String[] headers={"id","nombre","altura","peso","fecha matriculacion","posicion","club"};
        vista.dtm.setColumnIdentifiers(headers);
    }

    private void cargarFilas(ResultSet resultSet) throws SQLException {
        Object[] fila=new Object[7];
        vista.dtm.setRowCount(0);

        while (resultSet.next()){
            fila[0]=resultSet.getObject(1);
            fila[1]=resultSet.getObject(2);
            fila[2]=resultSet.getObject(3);
            fila[3]=resultSet.getObject(4);
            fila[4]=resultSet.getObject(5);
            fila[5]=resultSet.getObject(6);
            fila[6]=resultSet.getObject(7);

            vista.dtm.addRow(fila);
        }

        if(resultSet.last()) {
            vista.lblAccion.setVisible(true);
            vista.lblAccion.setText(resultSet.getRow()+" filas cargadas");
        }

    }

    private void iniciarTabla1(){
        String[] headers={"Club","Cuantos"};
        vista.dtm1.setColumnIdentifiers(headers);
    }

    private void cargarFilas1(ResultSet resultSet) throws SQLException {
        Object[] fila=new Object[2];
        vista.dtm1.setRowCount(0);

        while (resultSet.next()){
            fila[0]=resultSet.getObject(1);
            fila[1]=resultSet.getObject(2);

            vista.dtm1.addRow(fila);
        }

        if(resultSet.last()) {
            vista.lblAccion.setVisible(true);
            vista.lblAccion.setText(resultSet.getRow()+" filas cargadas");
        }
    }

    private void iniciarTabla2(){
        String[] headers={"id","nombre","altura","peso","fecha matriculacion","posicion","club"};
        vista.dtm2.setColumnIdentifiers(headers);
    }

    private void cargarFilas2(ResultSet resultSet) throws SQLException {
        Object[] fila=new Object[7];
        vista.dtm2.setRowCount(0);

        while (resultSet.next()){
            fila[0]=resultSet.getObject(1);
            fila[1]=resultSet.getObject(2);
            fila[2]=resultSet.getObject(3);
            fila[3]=resultSet.getObject(4);
            fila[4]=resultSet.getObject(5);
            fila[5]=resultSet.getObject(6);
            fila[6]=resultSet.getObject(7);

            vista.dtm2.addRow(fila);
        }

        if(resultSet.last()) {
            vista.lblAccion2.setVisible(true);
            vista.lblAccion2.setText(resultSet.getRow()+" filas cargadas");
        }
    }
}
