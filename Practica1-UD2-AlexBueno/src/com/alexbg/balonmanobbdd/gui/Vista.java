package com.alexbg.balonmanobbdd.gui;

import com.github.lgooddatepicker.components.DateTimePicker;

import javax.swing.*;
import javax.swing.table.DefaultTableModel;

public class Vista {
    public JPanel panel1;

    public JTextField txtNombre;
    public JTextField txtAltura;
    public JTextField txtPeso;
    public JTextField txtPosicion;
    public JTextField txtClub;
    public JTextField txtBuscar;
    public JTextField txtBuscarEquipo;
    public JTextField txtBuscarPosicion;

    public JButton buscarButton;
    public JButton nuevoButton;
    public JButton eliminarButton;
    public JButton jugadorPorClubButton;
    public JButton buscarPorEquipoYButton;

    public JLabel lblAccion;
    public JLabel lblAccion2;

    public DateTimePicker dateTimePicker;

    public JTable tabla;
    public JTable tabla1;
    public JTable tabla2;


    public DefaultTableModel dtm;
    public DefaultTableModel dtm1;
    public DefaultTableModel dtm2;

    public JMenuItem itemConectar;
    public JMenuItem itemCrearTabla;
    public JMenuItem itemSalir;
    public JFrame frame;

    public Vista() {
        frame = new JFrame("Balonmano BBDD");
        frame.setContentPane(panel1);
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

        dtm =new DefaultTableModel();
        tabla.setModel(dtm);

        dtm1 = new DefaultTableModel();
        tabla1.setModel(dtm1);

        dtm2 = new DefaultTableModel();
        tabla2.setModel(dtm2);

        crearMenu();

        frame.pack();
        frame.setVisible(true);
    }

    private void crearMenu() {
        itemConectar = new JMenuItem("Conectar");
        itemConectar.setActionCommand("Conectar");
        itemCrearTabla = new JMenuItem("Crear tabla Jugadores");
        itemCrearTabla.setActionCommand("CrearTablaJugadores");
        itemSalir = new JMenuItem("Salir");
        itemSalir.setActionCommand("Salir");

        JMenu menuArchivo = new JMenu("Archivo");
        menuArchivo.add(itemConectar);
        menuArchivo.add(itemCrearTabla);
        menuArchivo.add(itemSalir);
        JMenuBar barraMenu = new JMenuBar();
        barraMenu.add(menuArchivo);

        frame.setJMenuBar(barraMenu);
    }

    private void createUIComponents() {
        // TODO: place custom component creation code here
    }
}
