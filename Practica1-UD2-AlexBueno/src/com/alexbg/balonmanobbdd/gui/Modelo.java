package com.alexbg.balonmanobbdd.gui;

/**
 * Created by Alex on 26/12/20
 */

import javax.swing.*;
import java.sql.*;
import java.time.LocalDateTime;

public class Modelo {
    private Connection conexion;

    public void crearTablaJugadores() throws SQLException{
        conexion = null;
        conexion = DriverManager.getConnection("jdbc:mysql://localhost:3306/persona","root","");

        String sentenciaSQL = "call crearTablaJugadores()";
        CallableStatement procedimiento = null;
        procedimiento = conexion.prepareCall(sentenciaSQL);
        procedimiento.execute();
    }

    public void conectar() throws SQLException {
        conexion=null;
        conexion= DriverManager.getConnection("jdbc:mysql://localhost:3306/persona","root","");

    }

    public void desconectar() throws SQLException {
        conexion.close();
        conexion = null;
    }
    public ResultSet obtenerDatos() throws SQLException {
        if (conexion == null) {
            return null;
        }
        if (conexion.isClosed()) {
            return null;
        }
        String consulta ="SELECT * FROM jugadores";
        PreparedStatement sentencia = null;
        sentencia=conexion.prepareStatement(consulta);
        ResultSet resultado=sentencia.executeQuery();
        return resultado;
    }

    public ResultSet obtenerDatos1() throws SQLException {
        if (conexion==null) {
            return null;
        }
        if (conexion.isClosed()) {
            return null;
        }
        String consulta ="SELECT club, COUNT(*) FROM jugadores GROUP BY club";
        PreparedStatement sentencia = null;
        sentencia=conexion.prepareStatement(consulta);
        ResultSet resultado=sentencia.executeQuery();
        return resultado;
    }


    public int insertarPersona(String nombre, String altura, String peso, LocalDateTime fechaMatriculacion,
                               String posicion, String club) throws SQLException {
        if (conexion==null)
            return -1;
        if (conexion.isClosed())
            return -2;

        String consulta="INSERT INTO jugadores(nombre, altura, peso, fecha_Matriculacion,posicion, club)"+
                "VALUES (?,?,?,?,?,?)";

        PreparedStatement sentencia=null;

        sentencia=conexion.prepareStatement(consulta);

        sentencia.setString(1,nombre);
        sentencia.setString(2,altura);
        sentencia.setString(3, peso);
        sentencia.setTimestamp(4, Timestamp.valueOf(fechaMatriculacion));
        sentencia.setString(5, posicion);
        sentencia.setString(6,club);

        int numeroRegistros=sentencia.executeUpdate();

        if (sentencia!=null) {
            sentencia.close();
        }

        return numeroRegistros;
    }

    public int eliminarPersona(int id) throws SQLException {
        if (conexion==null)
            return -1;
        if (conexion.isClosed())
            return -2;

        String consulta="DELETE FROM jugadores WHERE id=?";
        PreparedStatement sentencia=null;

        sentencia=conexion.prepareStatement(consulta);
        sentencia.setInt(1,id);

        int resultado=sentencia.executeUpdate();

        if (sentencia!=null) {
            sentencia.close();
        }

        return resultado;

    }

    public int modificarPersona (int id, String nombre, String altura, String peso,
                                 Timestamp fechaMatriculacion, String posicion, String club) throws SQLException {
        if (conexion==null)
            return -1;
        if (conexion.isClosed())
            return -2;

        String consulta="UPDATE jugadores SET nombre=?, altura=?, peso=?,fecha_matriculacion=?,posicion=?, club=? WHERE id=?";

        PreparedStatement sentencia=null;

        sentencia=conexion.prepareStatement(consulta);

        sentencia.setString(1,nombre);
        sentencia.setString(2, altura);
        sentencia.setString(3, peso);
        sentencia.setTimestamp(4,fechaMatriculacion);
        sentencia.setString(5,posicion);
        sentencia.setString(6,club);
        sentencia.setInt(7,id);

        int resultado=sentencia.executeUpdate();

        if (sentencia!=null) {
            sentencia.close();
        }

        return resultado;
    }

    public ResultSet buscarJugador(String nombre) throws SQLException {
        if( conexion == null)
            return null;

        if( conexion.isClosed())
            return null;

        String consulta = "SELECT * FROM jugadores WHERE nombre = ?";
        PreparedStatement sentencia = null;

        sentencia = conexion.prepareStatement(consulta);
        sentencia.setString(1, String.valueOf(nombre));
        ResultSet resultado = sentencia.executeQuery();

        return resultado;
    }

    public void jugadoresPorClub() throws SQLException {
        String sentenciaSql="call mostrarJugadoresClub()";
        CallableStatement procedimiento=null;
        procedimiento=conexion.prepareCall(sentenciaSql);
        procedimiento.execute();
    }

    Vista vista = new Vista();
    public ResultSet jugadoresEquipo(String equipo, String puesto) throws SQLException {

        if( conexion == null)
            return null;

        if( conexion.isClosed())
            return null;
        String consulta = "";

        PreparedStatement sentencia;
        ResultSet resultado = null;

        if (vista.txtBuscarEquipo.equals(null) && vista.txtBuscarPosicion.equals(null)){
            consulta = "SELECT * FROM jugadores";
            sentencia = conexion.prepareStatement(consulta);
            resultado = sentencia.executeQuery();
        } else if (vista.txtBuscarEquipo.equals(null) && !vista.txtBuscarPosicion.equals(null)){
            consulta = "SELECT * FROM jugadores WHERE posicion=?";
            sentencia = conexion.prepareStatement(consulta);
            sentencia.setString(1, puesto);
            resultado = sentencia.executeQuery();
        } else if (!vista.txtBuscarEquipo.equals(null) && vista.txtBuscarPosicion.equals(null)){
            consulta = "SELECT * FROM jugadores WHERE club=?";
            sentencia = conexion.prepareStatement(consulta);
            sentencia.setString(1, equipo);
            resultado = sentencia.executeQuery();
        } else if (!vista.txtBuscarEquipo.equals(null) && !vista.txtBuscarPosicion.equals(null)){
            consulta = "SELECT * FROM jugadores WHERE club=? AND posicion=?";
            sentencia = conexion.prepareStatement(consulta);
            sentencia.setString(1, equipo);
            sentencia.setString(2, puesto);
            resultado = sentencia.executeQuery();
        } else {
            System.out.println("Error");
        }

        return resultado;
    }
}
