package com.alexbg.balonmanobbdd.gui;
/**
 * Created by Alex on 26/12/20
 */
public class Principal {
    public static void main(String[] args) {
        Vista vista = new Vista();
        Modelo modelo = new Modelo();
        Controlador controlador = new Controlador(vista, modelo);
    }
}
