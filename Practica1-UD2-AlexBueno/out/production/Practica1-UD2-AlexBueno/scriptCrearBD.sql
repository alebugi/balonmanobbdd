CREATE DATABASE persona;

use persona;

CREATE TABLE jugadores(
id int primary key auto_increment,
nombre varchar(30),
altura varchar(30),
peso varchar(30),
fecha_matriculacion timestamp,
posicion varchar(20),
club varchar(50)
);

INSERT INTO jugadores(nombre, altura, peso, fecha_matriculacion, posicion, club)
VALUES('Alex', '173', '87', '2006-09-06', 'extremo','Pol. SAN AGUSTIN');
select * from jugadores;